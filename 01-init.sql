-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Hostiteľ: mysql
-- Vygenerované: Št 03.Dec 2020, 22:40
-- Verzia serveru: 10.1.45-MariaDB
-- Verzia PHP: 5.6.40

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `platform_editor`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `AllowedAccount`
--

CREATE TABLE IF NOT EXISTS `AllowedAccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `EditorLanguage`
--

CREATE TABLE IF NOT EXISTS `EditorLanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `flag` varchar(5) NOT NULL,
  `name` varchar(24) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `factory_edit_trace`
--

CREATE TABLE IF NOT EXISTS `factory_edit_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_id` int(11) NOT NULL,
  `message` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41665 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `factory_module_account`
--

CREATE TABLE IF NOT EXISTS `factory_module_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `factory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `factory_id` (`factory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `factory_x_platform_account`
--

CREATE TABLE IF NOT EXISTS `factory_x_platform_account` (
  `factory_module_account_id` int(11) NOT NULL,
  `platform_account_id` int(11) NOT NULL,
  KEY `factory_module_account_id` (`factory_module_account_id`),
  KEY `platform_account_id` (`platform_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `Message`
--

CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`),
  KEY `id` (`id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `ordered_product_edit_log`
--

CREATE TABLE IF NOT EXISTS `ordered_product_edit_log` (
  `account_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `ordered_product_id` int(11) NOT NULL,
  `src_design_id` int(11) NOT NULL,
  `dst_design_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `created` (`created`),
  KEY `account_id` (`account_id`),
  KEY `shop_id` (`shop_id`),
  KEY `order_id` (`order_id`),
  KEY `ordered_product_id` (`ordered_product_id`),
  KEY `src_design_id` (`src_design_id`),
  KEY `dst_design_id` (`dst_design_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `platform_account`
--

CREATE TABLE IF NOT EXISTS `platform_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `SourceMessage`
--

CREATE TABLE IF NOT EXISTS `SourceMessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1969 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `update_motive_preset`
--

CREATE TABLE IF NOT EXISTS `update_motive_preset` (
  `user_id` int(11) NOT NULL,
  `items` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `update_product_preset`
--

CREATE TABLE IF NOT EXISTS `update_product_preset` (
  `user_id` int(11) NOT NULL,
  `items` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `user_permission`
--

CREATE TABLE IF NOT EXISTS `user_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(128) NOT NULL,
  `type` enum('ALL','READ','NONE') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`key`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4243 ;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `user_settings`
--

CREATE TABLE IF NOT EXISTS `user_settings` (
  `user_id` int(11) NOT NULL,
  `list_mode` enum('1','2') NOT NULL DEFAULT '1',
  `import_mode` enum('1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `ws_client_trace`
--

CREATE TABLE IF NOT EXISTS `ws_client_trace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` char(32) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `method` varchar(4) NOT NULL,
  `uri` varchar(2048) NOT NULL,
  `params` varchar(2048) NOT NULL,
  `is_exception` tinyint(1) NOT NULL DEFAULT '0',
  `exception` varchar(2048) NOT NULL,
  `is_wrong_format` tinyint(1) NOT NULL DEFAULT '0',
  `response` varchar(2048) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `session` (`session`),
  KEY `is_wrong_format` (`is_wrong_format`),
  KEY `is_exception` (`is_exception`),
  KEY `uri` (`uri`(255)),
  KEY `order_id` (`order_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `factory_edit_trace`
--
ALTER TABLE `factory_edit_trace`
  ADD CONSTRAINT `factory_edit_trace_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `factory_module_account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `factory_x_platform_account`
--
ALTER TABLE `factory_x_platform_account`
  ADD CONSTRAINT `factory_x_platform_account_ibfk_1` FOREIGN KEY (`factory_module_account_id`) REFERENCES `factory_module_account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `factory_x_platform_account_ibfk_2` FOREIGN KEY (`platform_account_id`) REFERENCES `platform_account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `Message`
--
ALTER TABLE `Message`
  ADD CONSTRAINT `FK_Message_SourceMessage` FOREIGN KEY (`id`) REFERENCES `SourceMessage` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
