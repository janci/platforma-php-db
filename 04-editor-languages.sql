-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Hostiteľ: mysql
-- Vygenerované: Pi 04.Dec 2020, 07:41
-- Verzia serveru: 10.1.45-MariaDB
-- Verzia PHP: 5.6.40

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáza: `platform_editor`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `EditorLanguage`
--

CREATE TABLE IF NOT EXISTS `EditorLanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `flag` varchar(5) NOT NULL,
  `name` varchar(24) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Sťahujem dáta pre tabuľku `EditorLanguage`
--

INSERT INTO `EditorLanguage` (`id`, `code`, `flag`, `name`, `default`) VALUES
(1, 'sk', 'sk', 'Slovenský', 0),
(2, 'en', 'en', 'English', 1),
(3, 'cs', 'cs', 'Česky', 0),
(4, 'pl', 'pl', 'Poľsky', 0),
(5, 'de', 'de', 'Deutch', 0);

