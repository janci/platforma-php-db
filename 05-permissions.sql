-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Hostiteľ: mysql
-- Vygenerované: Pi 04.Dec 2020, 07:58
-- Verzia serveru: 10.1.45-MariaDB
-- Verzia PHP: 5.6.40

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáza: `platform_editor`
--

--
-- Sťahujem dáta pre tabuľku `user_permission`
--

INSERT INTO `user_permission` (`id`, `user_id`, `key`, `type`) VALUES
(1, 1, 'core', 'ALL'),
(32, 1, 'core.production', 'ALL'),
(63, 1, 'core.production.printColors', 'ALL'),
(94, 1, 'core.production.printableMedia', 'ALL'),
(125, 1, 'core.production.printTechnologies', 'ALL'),
(156, 1, 'core.production.factoriesManagement', 'ALL'),
(187, 1, 'core.production.printTemplates', 'ALL'),
(218, 1, 'core.production.overviewSettings', 'ALL'),
(249, 1, 'core.products', 'ALL'),
(280, 1, 'core.products.productTypes', 'ALL'),
(311, 1, 'core.products.productAttributes', 'ALL'),
(342, 1, 'core.products.productCategories', 'ALL'),
(373, 1, 'core.products.productManagement', 'ALL'),
(404, 1, 'core.products.productManagement.properties', 'ALL'),
(435, 1, 'core.products.productManagement.properties.name', 'ALL'),
(466, 1, 'core.products.productManagement.properties.description', 'ALL'),
(497, 1, 'core.products.stickerManagement', 'ALL'),
(528, 1, 'core.motives', 'ALL'),
(559, 1, 'core.motives.motiveCategories', 'ALL'),
(590, 1, 'core.motives.motivesManagement', 'ALL'),
(621, 1, 'core.motives.motiveDesigners', 'ALL'),
(652, 1, 'core.motives.tagManagement', 'ALL'),
(683, 1, 'core.motives.designerMotiveInvoices', 'ALL'),
(714, 1, 'core.fontManager', 'ALL'),
(745, 1, 'core.generalSettings', 'ALL'),
(776, 1, 'core.generalSettings.countryLanguageManagement', 'ALL'),
(807, 1, 'core.generalSettings.currencyVat', 'ALL'),
(838, 1, 'core.pimpSetup', 'ALL'),
(869, 1, 'core.pimpSetup.account', 'ALL'),
(900, 1, 'core.pimpSetup.configurations', 'ALL'),
(931, 1, 'core.pimpSetup.statistics', 'ALL'),
(962, 1, 'shops', 'ALL'),
(993, 1, 'shops.shopManagement', 'ALL'),
(1024, 1, 'shops.shopManagement.createDeleteShop', 'ALL'),
(1055, 1, 'shops.shopManagement.shopSettings', 'ALL'),
(1086, 1, 'shops.shopManagement.products', 'ALL'),
(1117, 1, 'shops.shopManagement.productCategories', 'ALL'),
(1148, 1, 'shops.shopManagement.motives', 'ALL'),
(1179, 1, 'shops.shopManagement.motiveCategories', 'ALL'),
(1210, 1, 'shops.shopManagement.collections', 'ALL'),
(1241, 1, 'shops.shopManagement.collectionCategories', 'ALL'),
(1272, 1, 'shops.creatorSetup', 'ALL'),
(1303, 1, 'publisher', 'ALL'),
(1334, 1, 'publisher.products', 'ALL'),
(1365, 1, 'publisher.products.productsManagement', 'ALL'),
(1396, 1, 'publisher.products.productCategories', 'ALL'),
(1427, 1, 'publisher.motives', 'ALL'),
(1458, 1, 'publisher.motives.motivesManagement', 'ALL'),
(1489, 1, 'publisher.motives.motiveCategories', 'ALL'),
(1520, 1, 'administration', 'ALL'),
(1551, 1, 'administration.accountSettings', 'ALL'),
(1582, 1, 'administration.accountSettings.accountSettings', 'ALL'),
(1613, 1, 'administration.accountSettings.userManagement', 'ALL'),
(1644, 1, 'administration.devTools', 'ALL'),
(1675, 1, 'administration.orderManagement', 'ALL'),
(1706, 1, 'administration.availableProducts', 'ALL'),
(1737, 1, 'statistics', 'ALL'),
(1768, 1, 'core.products.productManagement.views', 'ALL'),
(1799, 1, 'core.products.productManagement.areasComponents', 'ALL'),
(1830, 1, 'core.products.productManagement.technology', 'ALL'),
(1861, 1, 'core.products.productManagement.stockProduction', 'ALL'),
(1892, 1, 'core.products.productManagement.views.viewAssign', 'ALL'),
(1923, 1, 'core.products.productManagement.views.colorTuning', 'ALL'),
(1954, 1, 'core.products.productManagement.views.subviews', 'ALL'),
(1985, 1, 'core.products.productManagement.views.productPictures', 'ALL'),
(2016, 1, 'core.products.productManagement.areasComponents.areas', 'ALL'),
(2047, 1, 'core.products.productManagement.areasComponents.collisionElements', 'ALL'),
(2078, 1, 'core.products.productManagement.areasComponents.viewPoints', 'ALL'),
(2109, 1, 'core.products.productManagement.areasComponents.overlays', 'ALL'),
(2140, 1, 'core.products.productManagement.areasComponents.humanView', 'ALL'),
(2171, 1, 'core.products.productManagement.technology.productions', 'ALL'),
(2202, 1, 'core.products.productManagement.technology.technologyAssigment', 'ALL'),
(2233, 1, 'core.products.productManagement.technology.technologyEnabled', 'ALL'),
(2264, 1, 'core.products.productManagement.technology.mediaSetup', 'ALL'),
(2295, 1, 'core.products.productManagement.technology.colorSetup', 'ALL'),
(2326, 1, 'core.products.productManagement.technology.plugins', 'ALL'),
(2357, 1, 'core.products.productManagement.stockProduction.availability', 'ALL'),
(2388, 1, 'core.products.productManagement.stockProduction.skus', 'ALL'),
(2419, 1, 'core.products.productManagement.properties.attributes', 'ALL'),
(2450, 1, 'core.products.productManagement.properties.customAttributes', 'ALL'),
(2481, 1, 'core.products.productManagement.properties.sizes', 'ALL'),
(2512, 1, 'core.products.productManagement.properties.colors', 'ALL'),
(2543, 1, 'core.products.productManagement.properties.price', 'ALL');

